FROM registry.gitlab.com/krillr-docker/archlinux-base:master
MAINTAINER Aaron Krill <aaron@krillr.com>

### Update mirror list
RUN curl -o /etc/pacman.d/mirrorlist "https://www.archlinux.org/mirrorlist/?country=US&protocol=https&ip_version=6&use_mirror_status=on" && \
  sed -i 's/^#//' /etc/pacman.d/mirrorlist

### Add infinality bundle
RUN echo "[infinality-bundle]" >> /etc/pacman.conf && \
    echo "Server = http://bohoomil.com/repo/\$arch" >> /etc/pacman.conf && \
    pacman-key -r 962DDE58 && pacman-key --lsign-key 962DDE58

### Set up the build user
RUN useradd -m -G wheel builder
WORKDIR /home/builder

### Switch to build user
USER builder

### Update system and install base desktop packages
RUN sudo pacman -Syu --noconfirm --noprogressbar xorg \
                                                 xorg-xinit \
                                                 xorg-xrandr \
                                                 alsa-utils \
                                                 mate-terminal \
                                                 zsh \
                                                 git \
                                                 vim \
                                                 tmux \
                                                 acpi \
                                                 sysstat \
                                                 yajl \
                                                 expac \
                                                 infinality-bundle && \
    sudo rm -rf /var/cache/pacman/pkg/*.xz

### Install pacaur and related deps
WORKDIR /tmp
RUN git clone http://aur.archlinux.org/cower-git.git && \
    cd cower-git && \
    makepkg && \
    sudo pacman -U --noconfirm /tmp/cower-git/cower-git-*.xz && \
    rm -rf /tmp/cower-git

RUN git clone http://aur.archlinux.org/pacaur-git.git && \
    cd pacaur-git && \
    makepkg && \
    sudo pacman -U --noconfirm /tmp/pacaur-git/pacaur-git-*.xz &&  \
    rm -rf /tmp/pacaur-git

### Tear down build user
USER root
WORKDIR /root
RUN userdel -r builder

